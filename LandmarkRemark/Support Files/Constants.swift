//
//  Constants.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 28/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import Foundation
import MapKit

struct Constants {
    
    static let RegionRadius:CLLocationDistance = 1000
    
    //the minimun space between keyboard and other UI Components
    static let KeyboardError: CGFloat = 8
    
    struct Segues {
        static let SignInToMainVC = "SignInToMainVC"
        static let MainVCToChangeUsernameVC = "MainToChangeUsername"
        static let MainVCToAddNoteVC = "MainVCToAddNoteVC"
    }

    struct ErrorMessages {
        static let LocationServiceRestrictedOrDenied = "You need to enable location service to use the main feature of the app"
        static let LocationServiceFailed = "Retrieving Location Failed, please try later"
        static let LackOfUsername = "You have not set your username"
    }
    
    struct ControllerIdentifier {
        static let NoteSearchTableViewControllerIdentifier = "NoteSearchTableViewController"
        static let NoteDetailViewControllerIdentifier = "DetailNoteVC"
        static let ChangeUsernameViewController = "ChangeUsernameViewController"
    }
    
    struct CellIdentifier {
        static let ItemTableViewCellIdentifier = "itemCell"
        static let ButtonTableViewCellIdentifier = "buttonCell"
    }
}
