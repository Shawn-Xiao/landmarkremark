//
//  CurrentLocation.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 2/3/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import Foundation
import MapKit


//User note model
class UserNote: NSObject, MKAnnotation {
    
    //whose note it is
    var title: String? {
        get {
            return "\(username)'s note"
        }
    }
    
//    if note content is less than 20 chars dipplay the whole note
//    otherwise display first 17 chars plus "..."
    var subtitle: String? {
        get {
            if note.count <= 20 {
                return note
            } else {
                return note.prefix(17) + "..."
            }
        }
    }
    
    var coordinate: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D.init(latitude: latitude, longitude: longitude)
        }
    }
    
    let latitude: Double
    let longitude: Double
    let note: String
    let username: String
    
    init(latitude: Double, longitude: Double, note: String, username: String) {
        self.latitude = latitude
        self.longitude = longitude
        self.note = note
        self.username = username
        super.init()
    }
}
