//
//  ProfileViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 2/3/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import Firebase

//Main Purpose: other associate functions list (change username & logout)
class ProfileViewController: UIViewController {
    
    var items = ["Username"]
    
    @IBOutlet weak var profileTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Profile"
        configureTableView()
    }
    
    private func configureTableView() {
        profileTableView.delegate = self
        profileTableView.dataSource = self
        profileTableView.register(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.ItemTableViewCellIdentifier)
        profileTableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.ButtonTableViewCellIdentifier)
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentIndex = indexPath.row
        switch currentIndex {
        case 0..<items.count:
            let cell = profileTableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! ItemTableViewCell
            cell.itemName.text = items[currentIndex]
            return cell
        default:
            let cell = profileTableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ButtonTableViewCell
            cell.buttonName.text = "Logout"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedIndex = indexPath.row
        switch selectedIndex {
        case 0..<items.count:
            if let nextController = storyboard!.instantiateViewController(withIdentifier: Constants.ControllerIdentifier.ChangeUsernameViewController) as? ChangeUsernameViewController {
                self.navigationController?.pushViewController(nextController, animated: true)
            }
        case items.count:
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                dismiss(animated: true, completion: nil)
            } catch let signOutError as NSError {
                print ("Error signing out: \(signOutError.localizedDescription)")
            }
        default:
            print("Out Of Indexpath")
        }
    }
}
