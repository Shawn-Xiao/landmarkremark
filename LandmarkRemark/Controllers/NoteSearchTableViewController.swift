//
//  LocationSearchTableViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 3/3/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import MapKit

//Main Purpose: search usernote based on note content and username of the note
class NoteSearchTableViewController: UITableViewController {
    var matchingNotes:[UserNote] = []
    var allNotes:[UserNote] = []
    var selectedNoteDelegate: SelectedNote?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingNotes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let currentNote = matchingNotes[indexPath.row]
        cell.textLabel?.text = currentNote.username
        cell.detailTextLabel?.text = currentNote.note
        cell.detailTextLabel?.lineBreakMode = .byTruncatingTail
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedNoteDelegate?.pinAndCenterOn(annotation: matchingNotes[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}

extension NoteSearchTableViewController:  UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchBarText = searchController.searchBar.text?.lowercased() else { return }
        matchingNotes = allNotes.filter({ (usernote) -> Bool in
            return usernote.username.lowercased().contains(searchBarText) || usernote.note.lowercased().contains(searchBarText)
        })
        tableView.reloadData()
    }
}
