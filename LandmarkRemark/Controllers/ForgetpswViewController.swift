//
//  ForgetpswViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 1/3/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import Firebase

//Main Purpose: send reset password email
class ForgetpswViewController: UIViewController {
    
    private var isKeyboardShown = false
    private var errorMessage: String? {
        didSet {
            if errorMessage != nil {
                errorMessageLabel.alpha = 1
                errorMessageLabel.text = errorMessage
            } else {
                errorMessageLabel.alpha = 0
                errorMessageLabel.text = errorMessage
            }
        }
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var resetFormContainer: UIStackView!
    
    
    @IBAction func backButtonTouched(_ sender: UIButton) {
        dismissKeyboard()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendResetEmail(_ sender: UIButton) {
        dismissKeyboard()
        resetPassword()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureKeyboard()
    }
    
    @objc func dismissKeyboard() {
        guard isKeyboardShown else {
            return
        }
        isKeyboardShown = false
        view.endEditing(true)
    }
    
    @objc func keyboardWillDismiss(notification: Notification) {
        UIView.animate(withDuration: 1) { [weak self] in
            self?.resetFormContainer.transform = CGAffineTransform.identity
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        guard !isKeyboardShown else {
            return
        }
        if let userInfo = notification.userInfo {
            if let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect{
                isKeyboardShown = true
                var moveDistance = keyboardFrame.origin.y - self.resetFormContainer.frame.maxY
                moveDistance = moveDistance > 0 ? 0 : moveDistance
                UIView.animate(withDuration: 1) { [weak self] in
                    self?.resetFormContainer.transform = CGAffineTransform.init(translationX: 0, y: moveDistance)
                }
            }
        }
    }
    
    //Helplers
    private func configureKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDismiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    private func resetPassword() {
        guard let email = emailTextField.text, !email.isEmpty else {
            errorMessage = "Please Fill In Your Email"
            return
        }
        Auth.auth().sendPasswordReset(withEmail: email) { [weak self] (error) in
            if error != nil {
                self?.errorMessage = "Failed To Send Reset Email"
            } else {
                let alert = UIAlertController.init(title: "Email Sent Successfully", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                    self?.dismiss(animated: true, completion: nil)
                }))
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
}
