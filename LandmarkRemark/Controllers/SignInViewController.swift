//
//  ViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 28/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController, UITextFieldDelegate {
    var isKeyboardShown = false
    var handle: AuthStateDidChangeListenerHandle?
    var errorMessage: String? {
        didSet {
            if errorMessage != nil {
                errorMessagesLabel.alpha = 1
                errorMessagesLabel.text = errorMessage
            } else {
                errorMessagesLabel.alpha = 0
                errorMessagesLabel.text = errorMessage
            }
        }
    }
    
    @IBOutlet weak var signInFormContainer: UIStackView!
    @IBOutlet weak var errorMessagesLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureKeyboard()
        configureAuthListener()
        configureDelegate()
    }
    
    @IBAction func signInButtonTouched(_ sender: UIButton) {
        dismissKeyboard()
        if isSignInFormFilled() {
            userSignIn()
        }
    }
    
    //if the shown keyboard will cover any other UI components, move up them to avoid being covered
    @objc func keyboardWillShow(notification: Notification) {
        guard !isKeyboardShown else {
            return
        }
        if let userInfo = notification.userInfo {
            if let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect{
                isKeyboardShown = true
                var moveDistance = keyboardFrame.origin.y - self.signInFormContainer.frame.maxY
                moveDistance = moveDistance > 0 ? 0 : moveDistance
                UIView.animate(withDuration: 1) { [weak self] in
                    self?.signInFormContainer.transform = CGAffineTransform.init(translationX: 0, y: moveDistance)
                }
            }
        }
    }
    
    @objc func keyboardWillDismiss(notification: Notification) {
        UIView.animate(withDuration: 1) { [weak self] in
            self?.signInFormContainer.transform = CGAffineTransform.identity
        }
    }
    
    @objc func dismissKeyboard() {
        guard isKeyboardShown else {
            return
        }
        isKeyboardShown = false
        view.endEditing(true)
    }
    
    //Helpers
    private func userSignIn() {
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { [weak self] (authDataResult, error) in
            if let authError = error as NSError? {
                switch authError.code {
                case AuthErrorCode.invalidEmail.rawValue:
                    self?.errorMessage = "Invalid Email Format"
                case AuthErrorCode.networkError.rawValue:
                    self?.errorMessage = "Network Error"
                case AuthErrorCode.wrongPassword.rawValue:
                    self?.errorMessage = "Wrong Password"
                case AuthErrorCode.userNotFound.rawValue:
                    self?.errorMessage = "User Not Found"
                default:
                    self?.errorMessage = "Sign In Failed"
                }
            }
        }
    }
    
    private func isSignInFormFilled() -> Bool {
        guard let email = emailTextField.text, !email.isEmpty else {
            errorMessage = "Please Enter Your Email"
            return false
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            errorMessage = "Please Enter Your Password"
            return false
        }
        return true
    }
    
    private func configureDelegate() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    private func configureAuthListener() {
        handle = Auth.auth().addStateDidChangeListener({ [weak self] (auth, user) in
            if user != nil {
                self?.performSegue(withIdentifier: Constants.Segues.SignInToMainVC, sender: self)
            }
        })
    }
    
    private func configureKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDismiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    
    //Remove Auth Listener
    deinit {
        if let handle = handle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
}

