//
//  MainViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 28/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import MapKit

//when usernote is selected in the search result, display and center on the annotation
protocol SelectedNote {
    func pinAndCenterOn(annotation: MKAnnotation)
}

// Main Purpose: display annotations and user location on the map
class MainViewController: UIViewController  {
    var noteSearchController: UISearchController?
    var noteSearchTableVC: NoteSearchTableViewController?
    private var ref: DatabaseReference!
    private var refHandle: DatabaseHandle?
    let locationManager = CLLocationManager()
    
    var usernotes = [UserNote]() {
        willSet {
            map.removeAnnotations(usernotes)
        }
        didSet {
            noteSearchTableVC?.allNotes = usernotes
            map.addAnnotations(usernotes)
        }
    }

    var currentLocation: CLLocation! {
        didSet {
            centerOn(location: currentLocation)
        }
    }
    
    @IBOutlet weak var map: MKMapView!
    
    //add note button touched
    @IBAction func createNoteButtonTouched(_ sender: UIButton) {
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        guard currentLocation != nil else {
            displayLocationServiceErrorAlert(errorMessage: Constants.ErrorMessages.LocationServiceFailed)
            return
        }
        
        guard currentUser.displayName != nil else {
            displayLackOfUsernameAlert()
            return
        }
        
        self.performSegue(withIdentifier: Constants.Segues.MainVCToAddNoteVC, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLocationService()
        configureFirebaseDatabase()
        configureNavigationController()
        configureSearchController()
        configureDelegate()
    }
    
    //stop location service and remove database observer
    deinit {
        locationManager.stopUpdatingLocation()
        if refHandle != nil  {
            self.ref.child("notes").removeObserver(withHandle: refHandle!)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.MainVCToAddNoteVC {
            let nextController = segue.destination as! AddNoteViewController
            nextController.currentLocation = currentLocation
        }
    }
    
    /*
     *  Helpers
     */
    
    
    private func configureDelegate() {
        map.delegate = self
    }
    
    //when user tries to add note and did not set username pop up this alert
    private func displayLackOfUsernameAlert() {
        let lackOfUsernameAlert = UIAlertController(title: "Reminder", message: Constants.ErrorMessages.LackOfUsername, preferredStyle: .alert)
        lackOfUsernameAlert.addAction(UIAlertAction(title: "Set it now", style: .default, handler: { [weak self] (_) in
            self?.performSegue(withIdentifier: Constants.Segues.MainVCToChangeUsernameVC, sender: self)
        }))
        lackOfUsernameAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(lackOfUsernameAlert, animated: true, completion: nil)
    }
    
    private func configureLocationService() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 100.0
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted,.denied:
            displayLocationServiceErrorAlert(errorMessage: Constants.ErrorMessages.LocationServiceRestrictedOrDenied)
            break
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        }
    }
    
    private func configureNavigationController() {
        self.navigationItem.title = "Landmark Remark"
    }
    
    private func configureFirebaseDatabase() {
        ref = Database.database().reference()
        refHandle = ref.child("notes").observe(.childAdded) { [weak self] (snapshot) in
            if let value = snapshot.value as? Dictionary<String, Any> {
                let altitude = value["altitude"] as! Double
                let longitutde = value["longitude"] as! Double
                let note = value["note"] as! String
                let username = value["username"] as! String
                let newAnnotation = UserNote.init(latitude: altitude, longitude: longitutde, note: note, username: username)
                self?.usernotes.append(newAnnotation)
            }
        }
    }
    
    private func configureSearchController() {
        noteSearchTableVC = storyboard!.instantiateViewController(withIdentifier: Constants.ControllerIdentifier.NoteSearchTableViewControllerIdentifier) as? NoteSearchTableViewController
        noteSearchTableVC!.selectedNoteDelegate = self
        noteSearchController = UISearchController(searchResultsController: noteSearchTableVC)
        noteSearchController?.searchResultsUpdater = noteSearchTableVC
        
        let searchBar = noteSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search for places"
        navigationItem.titleView = noteSearchController?.searchBar
        
        noteSearchController?.hidesNavigationBarDuringPresentation = false
        noteSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
    }
    
    private func displayLocationServiceErrorAlert(errorMessage: String) {
        let alert = UIAlertController(title: "Location Service Error", message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: false, completion: nil)
    }
    
    //center and focus on location
    private func centerOn(location: CLLocation) {
        map.setRegion(MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: Constants.RegionRadius, longitudinalMeters: Constants.RegionRadius), animated: true)
    }
}

extension MainViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted,.denied:
            displayLocationServiceErrorAlert(errorMessage: Constants.ErrorMessages.LocationServiceRestrictedOrDenied)
            break
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        displayLocationServiceErrorAlert(errorMessage: Constants.ErrorMessages.LocationServiceFailed)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastUpdatedLocation = locations.last {
            currentLocation = lastUpdatedLocation
        }
    }
}


extension MainViewController: MKMapViewDelegate {
    //my notes are displayed in black color
    //while others are displayed in red color
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
   
        guard let annotation = annotation as? UserNote else { return nil }
        
        var identifier = ""
        
        if let signedInUserName =  Auth.auth().currentUser?.displayName, annotation.username == signedInUserName  {
            identifier = "mynotes"
        } else {
            identifier = "othernotes"
        }
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: 0, y: 5)
            view.rightCalloutAccessoryView = UIButton.init(type: .detailDisclosure)
            view.animatesWhenAdded = true
        }
        if identifier == "mynotes" {
            view.markerTintColor = UIColor.black
        } else {
            view.markerTintColor = UIColor.red
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let annotation = view.annotation as? UserNote {
            if let detailNavController = storyboard?.instantiateViewController(withIdentifier: Constants.ControllerIdentifier.NoteDetailViewControllerIdentifier) as? NoteDetailViewController {
                detailNavController.note = annotation.note
                detailNavController.username = annotation.username
                self.navigationController?.pushViewController(detailNavController, animated: true)
            }
        }
    }
}

extension MainViewController: SelectedNote {
    func pinAndCenterOn(annotation: MKAnnotation) {
        centerOn(location: CLLocation.init(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude))
    }
}
