//
//  ChangeUsernameViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 2/3/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import Firebase

//main purpose: update username
class ChangeUsernameViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UITextField! {
        didSet {
            usernameLabel.text = Auth.auth().currentUser?.displayName
        }
    }
    
    @IBAction func updateUsernameTouched(_ sender: UIButton) {
        guard let username = usernameLabel.text, !username.isEmpty else {
            return
        }
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        changeRequest?.displayName = username
        changeRequest?.commitChanges(completion: { [weak self] (error) in
            if error != nil {
                let resultAlert = UIAlertController.init(title: "Fail to change Name", message: nil, preferredStyle: .alert)
                resultAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self?.present(resultAlert, animated: true, completion: nil)
            } else {
                let resultAlert = UIAlertController.init(title: "Change Name Successfully", message: nil, preferredStyle: .alert)
                resultAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    self?.navigationController?.popViewController(animated: true)
                }))
                self?.present(resultAlert, animated: true, completion: nil)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Username"
    }
}
