//
//  NoteDetailViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 3/3/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit

//Main Purpose: display the entire note
class NoteDetailViewController: UIViewController {
    
    var username = ""
    var note = ""
    @IBOutlet weak var noteTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "\(username)'s note"
        noteTextView.text = note
    }
}
