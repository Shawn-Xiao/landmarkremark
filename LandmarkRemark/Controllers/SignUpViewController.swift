//
//  SignUpViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 28/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {

    private var isKeyboardShown = false
    private var errorMessage: String? {
        didSet {
            if errorMessage != nil {
                errorMessageLabel.alpha = 1
                errorMessageLabel.text = errorMessage
            } else {
                errorMessageLabel.alpha = 0
                errorMessageLabel.text = errorMessage
            }
        }
    }
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var signUpFormContainer: UIStackView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var rePasswordTextField: UITextField!
    
    @IBAction func signUpButtonTouched(_ sender: UIButton) {
        dismissKeyboard()
        if isSignUpFormPreValidated() {
            userSignUp()
        }
    }
    
    @IBAction func backToSignInButtonTouched(_ sender: UIButton) {
        dismissKeyboard()
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureKeyboard()
    }
    
    
    @objc func dismissKeyboard() {
        guard isKeyboardShown else {
            return
        }
        isKeyboardShown = false
        view.endEditing(true)
    }
    
    @objc func keyboardWillDismiss(notification: Notification) {
        UIView.animate(withDuration: 1) { [weak self] in
            self?.signUpFormContainer.transform = CGAffineTransform.identity
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        guard !isKeyboardShown else {
            return
        }
        if let userInfo = notification.userInfo {
            if let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect{
                isKeyboardShown = true
                var moveDistance = keyboardFrame.origin.y - self.signUpFormContainer.frame.maxY
                moveDistance = moveDistance > 0 ? 0 : moveDistance
                UIView.animate(withDuration: 1) { [weak self] in
                    self?.signUpFormContainer.transform = CGAffineTransform.init(translationX: 0, y: moveDistance)
                }
            }
        }
    }
    
    
    /*
     *   Helpers
     */
    
    private func configureKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDismiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    
    private func userSignUp() {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { [weak self] (authDataResult, error) in
            if let currentError = error as NSError? {
                switch currentError.code {
                case AuthErrorCode.invalidEmail.rawValue:
                    self?.errorMessage = "Invalid Email Format"
                case AuthErrorCode.emailAlreadyInUse.rawValue:
                    self?.errorMessage = "Email Exists"
                case AuthErrorCode.weakPassword.rawValue:
                    self?.errorMessage = "Weak Password"
                case AuthErrorCode.networkError.rawValue:
                    self?.errorMessage = "Network Error"
                default:
                    self?.errorMessage = "Sign Up Failed"
                }
            } else {
                self?.performSegue(withIdentifier: Constants.Segues.SignInToMainVC, sender: self)
            }
        }
    }
  
    
    //check whether sign up form is empty or the two passwords are not matched
    private func isSignUpFormPreValidated() -> Bool {
        guard let email = emailTextField.text, !email.isEmpty else {
            errorMessage = "Please Enter Your Email"
            return false
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            errorMessage = "Please Enter Your Password"
            return false
        }
        
        guard let rePassword = rePasswordTextField.text, !rePassword.isEmpty else {
            errorMessage = "Please Re-Enter Your Password"
            return false
        }
        
        if password != rePassword {
            errorMessage = "Two Passwords Not Match"
            return false
        }
        return true
    }

}
