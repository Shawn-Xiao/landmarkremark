//
//  AddNoteViewController.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 2/3/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase

//Main Purpose: add usernote based on current location
class AddNoteViewController: UIViewController {
    
    var currentLocation: CLLocation!
    var ref: DatabaseReference!
    var username = Auth.auth().currentUser!.displayName!
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var noteTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addNoteButton: UIButton!
    @IBOutlet weak var noteTextView: UITextView! {
        didSet {
            noteTextView.layer.borderWidth = 1
            noteTextView.layer.borderColor = UIColor.black.cgColor
            noteTextView.layer.cornerRadius = 4
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        noteTextView.becomeFirstResponder()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        locationLabel.text = "Location: {\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)}"
        usernameLabel.text = "Username: \(username)"
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect{
            var moveDistance = keyboardFrame.origin.y - addNoteButton.frame.maxY - Constants.KeyboardError
            moveDistance = moveDistance > 0 ? 0 : moveDistance
            self.noteTextViewHeight.constant += moveDistance
        }
        
    }
    
    @IBAction func addNoteButtonTouched(_ sender: UIButton) {
        guard !noteTextView.text.isEmpty else {
            return
        }
        self.ref.child("notes").childByAutoId().setValue(["username": username, "note": noteTextView.text, "altitude": Double(currentLocation.coordinate.latitude), "longitude": Double(currentLocation.coordinate.longitude)]) { [weak self] (error, databaseReference) in
            let resultAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .alert)
            if error != nil {
                resultAlert.title = "Fail to add note"
                resultAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            } else {
                resultAlert.title = "Add note successfully"
                resultAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    self?.navigationController?.popViewController(animated: true)
                }))
            }
            self?.present(resultAlert, animated: true, completion: nil)
        }
    }
}
