//
//  ButtonTableViewCell.swift
//  LandmarkRemark
//
//  Created by Wenda Xiao on 4/3/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
